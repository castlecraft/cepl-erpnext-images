### Introduction

Refer: https://github.com/castlecraft/custom_containers

### Docker build

```shell
git clone https://github.com/frappe/frappe_docker
cd frappe_docker
source ../build.env
export APPS_JSON_BASE64=$(base64 -w 0 ../apps.json)
export IMAGE_NAME=${REGISTRY}/${PROJECT_NAMESPACE}/${PROJECT_NAME}/${IMAGE}:$(cat ../version.txt)
docker build \
  --build-arg=FRAPPE_PATH=${FRAPPE_REPO}\
  --build-arg=FRAPPE_BRANCH=${FRAPPE_VERSION}\
  --build-arg=PYTHON_VERSION=${PY_VERSION}\
  --build-arg=NODE_VERSION=${NODEJS_VERSION}\
  --build-arg=APPS_JSON_BASE64=${APPS_JSON_BASE64} \
  --tag=${IMAGE_NAME} \
  --file=images/custom/Containerfile .
docker push ${IMAGE_NAME}
```
